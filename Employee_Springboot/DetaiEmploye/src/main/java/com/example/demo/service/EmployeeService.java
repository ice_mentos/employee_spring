package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
//import com.example.demo.repository.EmployeeRepository;
//import org.graalvm.compiler.serviceprovider.ServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee){
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees){
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees(){
        return  repository.findAll();
    }

    public Employee getEmployeeById(int id){
        return repository.findById(id).orElse(null);
    }

    public Employee getEmployeeByName(String name){
        return repository.findByName(name);
    }

    public String deleteEmployee(int id){
        repository.deleteById(id);
        return "Employee removed!!";
    }

//    public Employee updateProduct(Employee employee) {
//        Employee existingEmployee = repository.findById(employee.getNik()).orElse(null);
//        existingEmployee.setNama(employee.getNama());
//        existingEmployee.setGolDarah(employee.getGolDarah());
//        existingEmployee.setAlamat(employee.getAlamat());
//        existingEmployee.setRt_rw(employee.getRt_rw());
//        existingEmployee.setKelurahan(employee.getKelurahan());
//        existingEmployee.setKecamatan(employee.getKecamatan());
//        existingEmployee.setAgama(employee.getAgama());
//        existingEmployee.setStatus(employee.getStatus());
//        existingEmployee.setPekerjaan(employee.getPekerjaan());
//        existingEmployee.setKewarganegaraan(employee.getKewarganegaraan());
//        return repository.save(existingEmployee);
//    }
}
