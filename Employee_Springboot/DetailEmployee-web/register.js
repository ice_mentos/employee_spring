function insertData(){
    let jsonReq = "";
    const nik = document.getElementById("nikInput").value;
    const nama = document.getElementById("namaInput").value;
    const temppatTgllahir = document.getElementById("tempattglInput").value;
    const jenisKelamin = document.getElementById("nikInput").value;
    const alamat = document.getElementById("alamatInput").value;
    const rtRw = document.getElementById("rtRwInput").value;
    const keluh = document.getElementById("keluhInput").value;
    const kecamatan = document.getElementById("kecamatanInput").value;
    const agama = document.getElementById("agamaInput").value;
    const status = document.getElementById("statusInput").value;
    const pekerjaan = document.getElementById("pekerjaanInput").value;
    const kewarganegaraan = document.getElementById("wargaInput").value;
    const golDarah = document.getElementById("golDarah").value;

    // (jenisKelamin != "Pilih.." && agama != "Pilih.." && status != "Pilih..")
  
        jsonReq = JSON.stringify({
            nik: parseInt(nik),
            name: nama,
            tempat_tgllahir: temppatTgllahir,
            gol_darah: golDarah,
            alamat: alamat,
            rt_rw: rtRw,
            kelurahan: keluh,
            kecamatan: kecamatan,
            jenis_kelamin: jenisKelamin,
            agama: agama,
            status: status,
            pekerjaan: pekerjaan,
            kewarganegaraan: kewarganegaraan,       
        })
        postData(jsonReq);
        // console.log(jsonReq);
        
    
}

function postData(body){
    fetch('http://localhost:6464/addEmployee', {
        method: 'POST',
        headers: {
            'Accept' : 'application/json, text/plain, */*',
            'Content-Type' : 'application/json'
        },
        body: body
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        console.log(data)
        window.location.href = "/index.html"
    })
    .catch(console.error);
}