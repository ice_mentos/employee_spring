function getData() {
    fetch(`http://localhost:6464/employees`)
        .then(response => response.json())
        .then(data => showData(data))
        .catch(console.error)
}

function getByidUpdate(id) {
    let resp;
    fetch(`http://localhost:6464/employees/${id}`)
        .then(response => response.json())
        .then(data => {
            showModalUpdate(data)
        })
        .catch(console.error)
} 

function showModalUpdate(res) {
    const dataEmployee = document.getElementById("dataEmployee");    
    dataEmployee.innerHTML += `<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Update Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <div class="mb-3">
        <label for="email">NIK </label>
        <input type="email" class="form-control" id="nikedit" placeholder="" name="nik" value="${res.nik}">
    </div>

    <div class="mb-3">
        <label for="address">Nama</label>
        <input type="text" class="form-control" id="namaedit" placeholder="" required name="nama">
    </div>

    <div class="mb-3">
        <label for="address2">Tempat,tgl lahir<span class="text-muted"> (Jakarta, 01-01-2001)</span></label>
        <input type="text" class="form-control" id="tempattgledit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Gol Darah<span class="text-muted"> (A,AB,B,O)</span></label>
        <input type="text" class="form-control" id="golDarahedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Alamat</label>
        <input type="text" class="form-control" id="alamatedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">RT/RW</label>
        <input type="text" class="form-control" id="rtRwedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Kelurahan</label>
        <input type="text" class="form-control" id="keluhedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Kecamatan</label>
        <input type="text" class="form-control" id="kecamatanedit" placeholder="">
    </div>
    <div class="mb-3">
        <label for="address2">Agama</label>
        <input type="text" class="form-control" id="agamaedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Status</label>
        <input type="text" class="form-control" id="statusedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Pekerjaan</label>
        <input type="text" class="form-control" id="pekerjaanedit" placeholder="">
    </div>

    <div class="mb-3">
        <label for="address2">Kewarganegaraan</label>
        <input type="text" class="form-control" id="wargaedit" placeholder="">
    </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save</button>
        </div>
        </div>
    </div>
    </div>`

    // const nik = document.getElementById("nikInput").value;
    // const nama = document.getElementById("namaInput").value;
    // const temppatTgllahir = document.getElementById("tempattglInput").value;
    // const jenisKelamin = document.getElementById("nikInput").value;
    // const alamat = document.getElementById("alamatInput").value;
    // const rtRw = document.getElementById("rtRwInput").value;
    // const keluh = document.getElementById("keluhInput").value;
    // const kecamatan = document.getElementById("kecamatanInput").value;
    // const agama = document.getElementById("agamaInput").value;
    // const status = document.getElementById("statusInput").value;
    // const pekerjaan = document.getElementById("pekerjaanInput").value;
    // const kewarganegaraan = document.getElementById("wargaInput").value;
    // const golDarah = document.getElementById("golDarah").value;


    // let jsonReq = JSON.stringify({
    //     nik: parseInt(nik),
    //     name: nama,
    //     tempat_tgllahir: temppatTgllahir,
    //     gol_darah: golDarah,
    //     alamat: alamat,
    //     rt_rw: rtRw,
    //     kelurahan: keluh,
    //     kecamatan: kecamatan,
    //     jenis_kelamin: jenisKelamin,
    //     agama: agama,
    //     status: status,
    //     pekerjaan: pekerjaan,
    //     kewarganegaraan: kewarganegaraan,       
    // })

    // fetch(`http://localhost:6464/update`, {
    //     method: 'PUT',
    //     headers: {
    //         'Accept' : 'application/json, text/plain, */*',
    //         'Content-Type' : 'application/json'
    //     },
    //     body: jsonReq
    // })
    // .then(data => console.log(data))
    // .catch(console.error);
}


getData();
 
const showData = (datas) => {
    datas.forEach(data => {
        console.log(data);
        const dataString = JSON.stringify(data);
        document.getElementById("dataEmployee").innerHTML +=
            `<div class="col-md-4">
        <div class="card mb-4 shadow-sm">
        <div class="card-body">
        <p class="card-text">${data.nik}</p>
        <p class="card-text">${data.name}</p>
        <p class="card-text">${data.tempat_tgllahir}</p>
        <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
        <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop">View</button>
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Detail Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <p class="card-text">NIK              :${data.nik}</p>
        <p class="card-text">Nama             :${data.name}</p>
        <p class="card-text">Tempat,tgl lahir :${data.tempat_tgllahir}</p>
        <p class="card-text">Jenis Kelamin    :${data.jenis_kelamin}</p>
        <p class="card-text">Alamat           :${data.alamat}</p>
        <p class="card-text">RT/RW            :${data.rt_rw}</p>
        <p class="card-text">Keluarahan        :${data.kelurahan}</p>
        <p class="card-text">Kecamatan  :${data.kecamatan}</p>
        <p class="card-text">Agama  :${data.agama}</p>
        <p class="card-text">Status  :${data.status}</p>
        <p class="card-text">Pekerjaan  :${data.pekerjaan}</p>
        <p class="card-text">Kewarnegaraan  :${data.kewarganegaraan}</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
    </div>
        <button type="button" class="btn btn-outline-primary" id="${data.nik}" data-toggle="modal" data-target="#exampleModalLong" onclick="getByidUpdate(id)">Update</button>
        <button type="button" class="btn btn-outline-danger" id="${data.nik}" onclick="hapus(id)">Delete</button>
     </div>
    </div>
</div>
</div>`
    });
}

function hapus(id) {
    console.log(id);
    var data = confirm("Apakah data akan dihapus ?")
    if(data){
        fetch(`http://localhost:6464/delete/`+id, {
            method : 'DELETE'
        })
        .then(response => location.reload())
        .catch(console.error)
    }
}
